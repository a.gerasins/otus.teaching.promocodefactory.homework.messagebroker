﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Application.Integration.Contracts;
using Otus.Teaching.Pcf.Administration.Core.Application.Services;


namespace Otus.Teaching.Pcf.Administration.Core.Application.Integration.Consumers
{
    public class PromoCodeCreatedEventConsumer: IConsumer<IPromoCodeCreatedEvent>
    {
        private readonly IEmployeeService _employeeService;

        public PromoCodeCreatedEventConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        public async Task Consume(ConsumeContext<IPromoCodeCreatedEvent> context)
        {
            if (context.Message.PartnerManagerId != null)
                await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}