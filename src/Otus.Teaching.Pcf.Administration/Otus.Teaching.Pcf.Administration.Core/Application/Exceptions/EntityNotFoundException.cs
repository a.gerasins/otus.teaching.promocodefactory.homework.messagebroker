﻿using System;

namespace Otus.Teaching.Pcf.Administration.Core.Application.Exceptions
{
    public class EntityNotFoundException: Exception
    {
        public EntityNotFoundException(Guid entityId) : base($"Entity {entityId} not found")
        {
        }
        
        public EntityNotFoundException(string? message) : base(message)
        {
        }

        public EntityNotFoundException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}