﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Contracts
{
    public class PromoCodeCreatedEvent
    {
        string ServiceInfo { get; set; }

        Guid PartnerId { get; set; }

        Guid PromoCodeId { get; set; }
        
        string PromoCode { get; set; }

        Guid PreferenceId { get; set; }

        string BeginDate { get; set; }

        string EndDate { get; set; }
        
        Guid? PartnerManagerId { get; set; }
    }
}